<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Admin Functionality
// Monday
Route::get('monday-index','Admin\MondayController@index');
Route::post('monday-save','Admin\MondayController@store');
Route::get('monday-edit/{id}','Admin\MondayController@edit');
Route::post('monday-update','Admin\MondayController@update');
Route::get('monday-delete/{id}','Admin\MondayController@destroy');

// Tuesday
Route::get('tuesday-index','Admin\TuesdayController@index');
Route::post('tuesday-save','Admin\TuesdayController@store');
Route::get('tuesday-edit/{id}','Admin\TuesdayController@edit');
Route::post('tuesday-update','Admin\TuesdayController@update');
Route::get('tuesday-delete/{id}','Admin\TuesdayController@destroy');

// Wednesday
Route::get('wednesday-index','Admin\WednesdayController@index');
Route::post('wednesday-save','Admin\WednesdayController@store');
Route::get('wednesday-edit/{id}','Admin\WednesdayController@edit');
Route::post('wednesday-update','Admin\WednesdayController@update');
Route::get('wednesday-delete/{id}','Admin\WednesdayController@destroy');

// Thrusday
Route::get('thrusday-index','Admin\ThrusdayController@index');
Route::post('thrusday-save','Admin\ThrusdayController@store');
Route::get('thrusday-edit/{id}','Admin\ThrusdayController@edit');
Route::post('thrusday-update','Admin\ThrusdayController@update');
Route::get('thrusday-delete/{id}','Admin\ThrusdayController@destroy');

// Friday
Route::get('friday-index','Admin\FridayController@index');
Route::post('friday-save','Admin\FridayController@store');
Route::get('friday-edit/{id}','Admin\FridayController@edit');
Route::post('friday-update','Admin\FridayController@update');
Route::get('friday-delete/{id}','Admin\FridayController@destroy');

// Saturday
Route::get('saturday-index','Admin\SaturdayController@index');
Route::post('saturday-save','Admin\SaturdayController@store');
Route::get('saturday-edit/{id}','Admin\SaturdayController@edit');
Route::post('saturday-update','Admin\SaturdayController@update');
Route::get('saturday-delete/{id}','Admin\SaturdayController@destroy');

// Sunday
Route::get('sunday-index','Admin\SundayController@index');
Route::post('sunday-save','Admin\SundayController@store');
Route::get('sunday-edit/{id}','Admin\SundayController@edit');
Route::post('sunday-update','Admin\SundayController@update');
Route::get('sunday-delete/{id}','Admin\SundayController@destroy');


// Dashboard Card
Route::get('m-index','CountController@mcount');
Route::get('t-index','CountController@tcount');
Route::get('w-index','CountController@wcount');
Route::get('th-index','CountController@thcount');
Route::get('f-index','CountController@fcount');
Route::get('s-index','CountController@scount');
Route::get('su-index','CountController@sucount');


// Front

Route::get('m-front','FrontController@mfront');
Route::get('t-front','FrontController@tfront');
Route::get('w-front','FrontController@wfront');
Route::get('th-front','FrontController@thfront');
Route::get('f-front','FrontController@ffront');
Route::get('s-front','FrontController@sfront');
Route::get('su-front','FrontController@sufront');

// Edit
Route::get('m-edit/{id}','FrontController@edit');
Route::get('t-edit/{id}','FrontController@tedit');
Route::get('w-edit/{id}','FrontController@wedit');
Route::get('th-edit/{id}','FrontController@thedit');
Route::get('f-edit/{id}','FrontController@fedit');
Route::get('s-edit/{id}','FrontController@sedit');
Route::get('su-edit/{id}','FrontController@suedit');


// Search Controller 
Route::post('search','SearchController@index');