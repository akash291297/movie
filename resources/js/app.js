/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Navigation from './components/NavbarComponent.vue';
import Sidebar from './components/SidebarComponent.vue';
import router from './router.js';

Vue.component('pagination', require('laravel-vue-pagination'));
const app = new Vue({
    el: '#app',
    router,
    components:{
    	'Navigation':Navigation,
    	'Sidebar':Sidebar
    }
});

