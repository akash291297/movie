import Vue from 'vue'
import VueRouter from 'vue-router'
import Sunday from './components/Content/SundayComponent.vue';
import Monday from './components/Content/MondayComponent.vue';
import Tuesday from './components/Content/TuesdayComponent.vue';
import Wednesday from './components/Content/WednesdayComponent.vue';
import Thrusday from './components/Content/ThrusdayComponent.vue';
import Friday from './components/Content/FridayComponent.vue';
import Saturday from './components/Content/SaturdayComponent.vue';
import BookComponent from './components/Content/BookComponent.vue';
import BookComponent1 from './components/Content/BookComponent1.vue';
import BookComponent2 from './components/Content/BookComponent2.vue';
import BookComponent3 from './components/Content/BookComponent3.vue';
import BookComponent4 from './components/Content/BookComponent4.vue';
import BookComponent5 from './components/Content/BookComponent5.vue';
import BookComponent6 from './components/Content/BookComponent6.vue';
import DashboardComponent from './components/Admin/DashboardComponent.vue';
import LoginComponent from './components/LoginComponent.vue';
import RegisterComponent from './components/RegisterComponent.vue';
import CreateComponent from './components/Admin/Monday/CreateComponent.vue';
import IndexComponent from './components/Admin/Monday/IndexComponent.vue';
import EditComponent from './components/Admin/Monday/EditComponent.vue';
// Tuesday
import TuesdayCreateComponent from './components/Admin/Tuesday/CreateComponent.vue';
import TuesdayIndexComponent from './components/Admin/Tuesday/IndexComponent.vue';
import TuesdayEditComponent from './components/Admin/Tuesday/EditComponent.vue';

// Wednesday
import WednesdayCreateComponent from './components/Admin/Wednesday/CreateComponent.vue';
import WednesdayIndexComponent from './components/Admin/Wednesday/IndexComponent.vue';
import WednesdayEditComponent from './components/Admin/Wednesday/EditComponent.vue';

// Thrusady
import ThrusdayCreateComponent from './components/Admin/Thrusday/CreateComponent.vue';
import ThrusdayIndexComponent from './components/Admin/Thrusday/IndexComponent.vue';
import ThrusdayEditComponent from './components/Admin/Thrusday/EditComponent.vue';

// Friday
import FridayCreateComponent from './components/Admin/Friday/CreateComponent.vue';
import FridayIndexComponent from './components/Admin/Friday/IndexComponent.vue';
import FridayEditComponent from './components/Admin/Friday/EditComponent.vue';

// Saturday
import SaturdayCreateComponent from './components/Admin/Saturday/CreateComponent.vue';
import SaturdayIndexComponent from './components/Admin/Saturday/IndexComponent.vue';
import SaturdayEditComponent from './components/Admin/Saturday/EditComponent.vue';

// Sunday

import SundayCreateComponent from './components/Admin/Sunday/CreateComponent.vue';
import SundayIndexComponent from './components/Admin/Sunday/IndexComponent.vue';
import SundayEditComponent from './components/Admin/Sunday/EditComponent.vue';
Vue.use(VueRouter)

const routes =[
	
	{
		path:'/sunday',
		component: Sunday
	},
	{
		path:'/monday',
		component:Monday
	},
	{
		path:'/tuesday',
		component:Tuesday
	},
	{
		path:'/wednesday',
		component:Wednesday
	},
	{
		path:'/thrusday',
		component:Thrusday
	},
	{
		path:'/friday',
		component:Friday
	},
	{
		path:'/saturday',
		component:Saturday
	},
	{
		path:'/dashboard',
		component:DashboardComponent
	},
	{
		path:'/login',
		component:LoginComponent
	},
	{
		path:'/register',
		component:RegisterComponent
	},
	{
		path:'/Mondays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:CreateComponent
			}
			
		]
	},
	{
		path:'/Mondays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:IndexComponent
			}
		]
	},
	{
		path:'/Mondays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:EditComponent,
				name:'EditMonday',
			}
		]
	},
	{
		path:'/Tuesdays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:TuesdayCreateComponent
			}
		]
	},
	{
		path:'/Tuesdays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:TuesdayIndexComponent
			}
		]
	},
	{
		path:'/Tuesdays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:TuesdayEditComponent,
				name:'EditTuesday'
			}
		]
	},
	{
		path:'/Wednesdays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:WednesdayCreateComponent
			}
		]
	},
	{
		path:'/Wednesdays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:WednesdayIndexComponent
			}
		]
	},
	{
		path:'/Wednesdays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:WednesdayEditComponent,
				name:'EditWednesday'
			}
		]
	},
	{
		path:'/Thrusdays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:ThrusdayCreateComponent
			}
		]
	},
	{
		path:'/Thrusdays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:ThrusdayIndexComponent
			}
		]
	},
	{
		path:'/Thrusdays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:ThrusdayEditComponent,
				name:'EditThrusday'
			}
		]
	},
	{
		path:'/Fridays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:FridayCreateComponent
			}
		]
	},
	{
		path:'/Fridays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:FridayIndexComponent
			}
		]
	},
	{
		path:'/Fridays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:FridayEditComponent,
				name:'EditFriday'
			}
		]
	},
	{
		path:'/Saturdays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:SaturdayCreateComponent
			}
		]
	},
	{
		path:'/Saturdays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:SaturdayIndexComponent
			}
		]
	},
	{
		path:'/Saturdays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:SaturdayEditComponent,
				name:'EditSaturday'
			}
		]
	},
	{
		path:'/Sundays',
		component:DashboardComponent,
		children:[
			{
				path:'create',
				component:SundayCreateComponent
			}
		]
	},
	{
		path:'/Sundays',
		component:DashboardComponent,
		children:[
			{
				path:'index',
				component:SundayIndexComponent
			}
		]
	},
	{
		path:'/Sundays',
		component:DashboardComponent,
		children:[
			{
				path:'edit/:id',
				component:SundayEditComponent,
				name:'EditSunday'
			}
		]
	},
	{
		path:'/Book/edit/:id',
		component:BookComponent,
		name:'BookMovie'
	},
	{
		path:'/Book1/edit/:id',
		component:BookComponent1,
		name:'BookMovie1'
	},
	{
		path:'/Book2/edit/:id',
		component:BookComponent2,
		name:'BookMovie2'
	},
	{
		path:'/Book3/edit/:id',
		component:BookComponent3,
		name:'BookMovie3'
	},
	{
		path:'/Book4/edit/:id',
		component:BookComponent4,
		name:'BookMovie4'
	},
	{
		path:'/Book5/edit/:id',
		component:BookComponent5,
		name:'BookMovie5'
	},
	{
		path:'/Book6/edit/:id',
		component:BookComponent6,
		name:'BookMovie6'
	}


];

const router = new VueRouter({routes})

export default router