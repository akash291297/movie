<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tuesday extends Model
{
    protected $table= "tuesday";
    public $timestamps = false;
}
