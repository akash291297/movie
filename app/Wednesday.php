<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wednesday extends Model
{
    protected $table= "wednesday";
    public $timestamps = false;
}
