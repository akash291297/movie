<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monday;
use App\Tuesday;
use App\Wednesday;
use App\Thrusday;
use App\Friday;
use App\Saturday;
use App\Sunday;

class CountController extends Controller
{
    public function mcount(){
    	 $mondayList = Monday::orderBy('id','desc')->get();
        if($mondayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $mondayList;
        return Response()->json($mondayList,200);
    }

    public function tcount(){
    	$fridayList = Tuesday::orderBy('id','desc')->get();
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function wcount()
    {
        $fridayList = Wednesday::orderBy('id','desc')->get();
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function thcount()
    {
        $fridayList = Thrusday::orderBy('id','desc')->get();
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function fcount()
    {
        $fridayList = Friday::orderBy('id','desc')->get();
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function scount()
    {
        $saturdayList = Saturday::orderBy('id','desc')->get();
        if($saturdayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $saturdayList;
        return Response()->json($saturdayList,200);
    }

    public function sucount(){
        $sundayList = Sunday::orderBy('id','desc')->get();
        if($sundayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $sundayList;
        return Response()->json($sundayList,200);
    }
}
