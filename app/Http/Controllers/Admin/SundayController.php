<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sunday;
use Validator;
class SundayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sundayList = Sunday::orderBy('id','desc')->paginate(10);
        if($sundayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $sundayList;
        return Response()->json($sundayList,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"  => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"           => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){    
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $sundayRegister              = new Sunday();
        $sundayRegister->name        = $request->name;
        $sundayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $sundayRegister->image = $name;
        }
        $sundayRegister->show_time   = strtotime($request->show_time);
        $sundayRegister->category    = $request->category;
        $sundayRegister->rupees      = $request->price;
        $sundayRegister->save();
        $response['message']         = "Data Inserted Successfully";
        $response['data']            = $sundayRegister;
        return Response()->json($response,200);
    



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sundayEdit = Sunday::find($id);
        if($sundayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $sundayEdit;
        return Response()->json($sundayEdit,200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            "sunday_id"   => "required|numeric",
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"    => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"             => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $sundayRegister              = Sunday::find($request->sunday_id);
        $sundayRegister->name        = $request->name;
        $sundayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $sundayRegister->image = $name;
        }
        $sundayRegister->show_time   = strtotime($request->show_time);
        $sundayRegister->category    = $request->category;
        $sundayRegister->rupees      = $request->price;
        $sundayRegister->save();
        $response['message']         = "Data Updated Successfully";
        $response['data']            = $sundayRegister;
        return Response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sundayDelete = Sunday::find($id);
        $sundayDelete->delete();
        $response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $sundayDelete;
        if($sundayDelete){
            $fridayNew = Sunday::orderBy('id','desc')->paginate(10);
            $response['return'] = true;
            $response['data']   = $fridayNew;
            return Response()->json($fridayNew,200);

        }
    }
}
