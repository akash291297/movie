<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Wednesday;
use Validator;

class WednesdayController extends Controller
{/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fridayList = Wednesday::orderBy('id','desc')->paginate(10);
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"  => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"           => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){    
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $fridayRegister              = new Wednesday();
        $fridayRegister->name        = $request->name;
        $fridayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $fridayRegister->image = $name;
        }
        $fridayRegister->show_time   = strtotime($request->show_time);
        $fridayRegister->category    = $request->category;
        $fridayRegister->rupees      = $request->price;
        $fridayRegister->save();
        $response['message']         = "Data Inserted Successfully";
        $response['data']            = $fridayRegister;
        return Response()->json($response,200);
    



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fridayEdit = Wednesday::find($id);
        if($fridayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $fridayEdit;
        return Response()->json($fridayEdit,200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            "wednesday_id"   => "required|numeric",
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"    => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"             => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $fridayRegister              = Wednesday::find($request->wednesday_id);
        $fridayRegister->name        = $request->name;
        $fridayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $fridayRegister->image = $name;
        }
        $fridayRegister->show_time   = strtotime($request->show_time);
        $fridayRegister->category    = $request->category;
        $fridayRegister->rupees      = $request->price;
        $fridayRegister->save();
        $response['message']         = "Data Updated Successfully";
        $response['data']            = $fridayRegister;
        return Response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fridayDelete = Wednesday::find($id);
        $fridayDelete->delete();
        $response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $fridayDelete;
        if($fridayDelete){
            $fridayNew = Wednesday::orderBy('id','desc')->paginate(10);
            $response['return'] = true;
            $response['data']   = $fridayNew;
            return Response()->json($fridayNew,200);

        }
    }
}
