<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Monday;
use Validator;

class MondayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mondayList = Monday::orderBy('id','desc')->paginate(2);
        if($mondayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $mondayList;
        return Response()->json($mondayList,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"    => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"           => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $mondayRegister              = new Monday();
        $mondayRegister->name        = $request->name;
        $mondayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $mondayRegister->image = $name;
        }
        $mondayRegister->show_time   = strtotime($request->show_time);
        $mondayRegister->category    = $request->category;
        $mondayRegister->rupees      = $request->price;
        $mondayRegister->save();
        $response['message']         = "Data Inserted Successfully";
        $response['data']            = $mondayRegister;
        return Response()->json($response,200);
    



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mondayEdit = Monday::find($id);
        if($mondayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $mondayEdit;
        return Response()->json($mondayEdit,200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            "monday_id"   => "required|numeric",
            "name"        => "required",
            "description" => "required",
            "show_time"   => "required",
            "category"    => "required",
            "price"       => "required"
        ];

        $errors = [
            "name.required"        => "Enter Movie Name",
            "description.required" => "Enter Movie Description",
            "show_time"            => "Enter Movie Time",
            "category"             => "Choose Movie Type",
            "price .required"      => "Enter Ticket Price"
        ];

        
            $Validator = Validator::make($request->all(),$rules,$errors);
            if($Validator->fails()){
            $keys = array_keys($Validator->getMessageBag()->toArray());
            $errors = $Validator->getMessageBag()->toArray();
            $response['errors'] = $errors;
            $response['error_keys'] = $keys;
            return Response()->json($response,400);
            }  

        $mondayRegister              = Monday::find($request->monday_id);
        $mondayRegister->name        = $request->name;
        $mondayRegister->description = $request->description;
        if($request->get('image'))
       {
          $image = $request->get('image');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('image'))->fit(200,150)->save(public_path('images/').$name);


        
            $mondayRegister->image = $name;
        }
        $mondayRegister->show_time   = strtotime($request->show_time);
        $mondayRegister->category    = $request->category;
        $mondayRegister->rupees      = $request->price;
        $mondayRegister->save();
        $response['message']         = "Data Updated Successfully";
        $response['data']            = $mondayRegister;
        return Response()->json($response,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mondayDelete = Monday::find($id);
        $mondayDelete->delete();
        $response['return']  =  true;
        $response['message'] = 'Record Deleted Successfully';
        $response['deleted data']= $mondayDelete;
        if($mondayDelete){
            $mondayNew = Monday::orderBy('id','desc')->paginate(2);
            $response['return'] = true;
            $response['data']   = $mondayNew;
            return Response()->json($mondayNew,200);

        }
    }
}
