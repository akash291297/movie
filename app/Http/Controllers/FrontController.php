<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monday;
use App\Tuesday;
use App\Wednesday;
use App\Thrusday;
use App\Friday;
use App\Saturday;
use App\Sunday;
class FrontController extends Controller
{
    public function mfront(){
    	 $mondayList = Monday::orderBy('id','desc')->paginate(2);
        if($mondayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $mondayList;
        return Response()->json($mondayList,200);
    }

    public function edit($id)
    {
        $mondayEdit = Monday::find($id);
        if($mondayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $mondayEdit;
        return Response()->json($mondayEdit,200);

    }

    public function tfront(){
    	$fridayList = Tuesday::orderBy('id','desc')->paginate(10);
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function tedit($id){
        $fridayEdit = Tuesday::find($id);
        if($fridayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $fridayEdit;
        return Response()->json($fridayEdit,200);
    }

    public function wfront()
    {
        $fridayList = Wednesday::orderBy('id','desc')->paginate(10);
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }
    public function wedit($id)
    {
        $fridayEdit = Wednesday::find($id);
        if($fridayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $fridayEdit;
        return Response()->json($fridayEdit,200);

    }
    public function thfront()
    {
        $fridayList = Thrusday::orderBy('id','desc')->paginate(10);
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }

    public function thedit($id)
    {
        $fridayEdit = Thrusday::find($id);
        if($fridayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $fridayEdit;
        return Response()->json($fridayEdit,200);

    }

    public function ffront()
    {
        $fridayList = Friday::orderBy('id','desc')->paginate(10);
        if($fridayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $fridayList;
        return Response()->json($fridayList,200);
    }
    public function fedit($id)
    {
        $fridayEdit = Friday::find($id);
        if($fridayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $fridayEdit;
        return Response()->json($fridayEdit,200);

    }
    public function sfront()
    {
        $saturdayList = Saturday::orderBy('id','desc')->paginate(10);
        if($saturdayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $saturdayList;
        return Response()->json($saturdayList,200);
    }
    public function sedit($id)
    {
        $saturdayEdit = Saturday::find($id);
        if($saturdayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $saturdayEdit;
        return Response()->json($saturdayEdit,200);

    }
    public function sufront(){
        $sundayList = Sunday::orderBy('id','desc')->paginate(10);
        if($sundayList == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

        }
        $response['return'] = true;
        $response['data']   = $sundayList;
        return Response()->json($sundayList,200);
    }

    public function suedit($id)
    {
        $sundayEdit = Sunday::find($id);
        if($sundayEdit == null){
            $response['return'] = false;
            $response['message'] =" Data Not Found";
            return Response()->json($response,200);

        }

        $response['return'] = true;
        $response['data'] = $sundayEdit;
        return Response()->json($sundayEdit,200);

    }
}
