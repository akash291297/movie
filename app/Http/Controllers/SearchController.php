<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monday;
use App\Tuesday;
use App\Wednesday;
use App\Thrusday;
use App\Friday;
use App\Saturday;
use App\Sunday;
use DB;

class SearchController extends Controller
{
    public function index(Request $request){

    	$all = DB::table('monday')
    	   ->join('tuesday','monday.category' ,'=','tuesday.category')
    	   ->where('monday.category',$request->boxd )
    	   ->where('tuesday.category',$request->boxd )
		   ->select('monday.*','tuesday.*')
    	   ->get();
    	   if($all == null){
            $response['return'] = false;
            $response['message'] = "Data Not Found";
            return Response()->json($response,400);

	        }
	        $response['return'] = true;
	        $response['data']   = $all;
	        return Response()->json($all,200);

    }
    
}
