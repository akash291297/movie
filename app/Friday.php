<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friday extends Model
{
    protected $table= "friday";
    public $timestamps = false;
}
