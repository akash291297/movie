<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saturday extends Model
{
    protected $table= "saturday";
    public $timestamps = false;
}
