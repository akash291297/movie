<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monday extends Model
{
   protected $table= "monday";
   public $timestamps = false;
}
