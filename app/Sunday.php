<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sunday extends Model
{
    protected $table= "sunday";
    public $timestamps = false;
}
